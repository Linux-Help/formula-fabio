{%- from slspath + '/map.jinja' import fabio with context -%}

fabio-config:
  file.managed:
    - name: {{ fabio.config_file }}
    - source: salt://{{ slspath }}/files/fabio.cfg
    - context:
        config: {{ fabio.config }}
    - template: jinja
    - user: {{ fabio.user}}
    - group: {{ fabio.group }}
    - mode: 0640
    - require:
      - user: fabio-user
    {%- if fabio.service %}
    - watch_in:
      - service: fabio
    {%- endif %}
  
{#
fabio-config:
  file.serialize:
    - name: /etc/fabio/config.json
    - formatter: json
    - dataset: {{ fabio.config }}
    - user: {{ fabio.user }}
    - group: {{ fabio.group }}
    - mode: 0640
    - require:
      - user: fabio-user
    {%- if fabio.service %}
    - watch_in:
       - service: fabio
    {%- endif %}

{% for script in fabio.scripts %}
fabio-script-install-{{ loop.index }}:
  file.managed:
    - source: {{ script.source }}
    - name: {{ script.name }}
    - template: jinja
    - context: {{ script.get('context', {}) | yaml }}
    - user: {{ fabio.user }}
    - group: {{ fabio.group }}
    - mode: 0755
{% endfor %}

fabio-script-config:
  file.serialize:
    - name: /etc/fabio.d/services.json
    {% if fabio.service != False %}
    - watch_in:
       - service: fabio
    {% endif %}
    - user: {{ fabio.user }}
    - group: {{ fabio.group }}
    - require:
      - user: fabio-user
    - formatter: json
    - dataset:
        services: {{ fabio.register }}
#}
