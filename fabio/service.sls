{%- from slspath+"/map.jinja" import fabio with context -%}

fabio-init-env:
  file.managed:
    {%- if grains['os_family'] == 'Debian' %}
    - name: /etc/default/fabio
    {%- else %}
    - name: /etc/sysconfig/fabio
    - makedirs: True
    {%- endif %}
    - user: root
    - group: root
    - mode: 0644
    - contents:
      - FABIO_USER={{ fabio.user }}
      - FABIO_GROUP={{ fabio.group }}
      - GOMAXPROCS=2
      - PATH=/usr/local/bin:/usr/bin:/bin
      {% for k, v in salt['pillar.get']('fabio:env', {}).items() %}
      - {{ k }}={{ v }}
      {% endfor %}

fabio-init-file:
  file.managed:
    {%- if salt['test.provider']('service') == 'systemd' %}
    - source: salt://{{ slspath }}/files/fabio.service
    - name: /etc/systemd/system/fabio.service
    - template: jinja
    - context:
        user: {{ fabio.user }}
        group: {{ fabio.group }}
        config_file: {{ fabio.config_file }}
    - mode: 0644
    {%- elif salt['test.provider']('service') == 'upstart' %}
    - source: salt://{{ slspath }}/files/fabio.upstart
    - name: /etc/init/fabio.conf
    - mode: 0644
    {%- else %}
    - source: salt://{{ slspath }}/files/fabio.sysvinit
    - name: /etc/init.d/fabio
    - mode: 0755
    {%- endif %}

{%- if fabio.service %}

fabio-service:
  service.running:
    - name: fabio
    - enable: True
    - watch:
      - file: fabio-init-env
      - file: fabio-init-file

{%- endif %}
