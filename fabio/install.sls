{%- from slspath + '/map.jinja' import fabio with context -%}

fabio-bin-dir:
  file.directory:
    - name: /usr/local/bin
    - makedirs: True

# Create fabio user
fabio-group:
  group.present:
    - name: {{ fabio.group }}

fabio-user:
  user.present:
    - name: {{ fabio.user }}
    - groups:
      - {{ fabio.group }}
    - home: {{ salt['user.info'](fabio.user)['home']|default(fabio.data_dir) }}
    - createhome: False
    - system: True
    - require:
      - group: fabio-group

# Create directories
fabio-config-dir:
  file.directory:
    - name: /etc/fabio
    - user: {{ fabio.user }}
    - group: {{ fabio.group }}
    - mode: 0750

fabio-data-dir:
  file.directory:
    - name: {{ fabio.data_dir }}
    - makedirs: True
    - user: {{ fabio.user }}
    - group: {{ fabio.group }}
    - mode: 0750

# Install agent
fabio-download:
  file.managed:
    - name: /tmp/fabio-{{ fabio.version }}
    - source: https://{{ fabio.download_host }}/fabiolb/fabio/releases/download/v{{ fabio.version }}/fabio-{{ fabio.version }}-go{{ fabio.goversion }}-linux_{{ fabio.arch }}
    - source_hash: https://github.com/fabiolb/fabio/releases/download/v{{ fabio.version }}/fabio-{{ fabio.version }}-go{{ fabio.goversion }}.sha256
    - mode: 0755
    - unless: test -f /usr/local/bin/fabio-{{ fabio.version }}

fabio-install:
  file.rename:
    - name: /usr/local/bin/fabio-{{ fabio.version }}
    - source: /tmp/fabio-{{ fabio.version }}
    - require:
      - file: /usr/local/bin
    - watch:
      - file: fabio-download
    
fabio-clean:
  file.absent:
    - name: /tmp/fabio-{{ fabio.version }}
    - watch:
      - file: fabio-install

fabio-link:
  file.symlink:
    - target: fabio-{{ fabio.version }}
    - name: /usr/local/bin/fabio
    - watch:
      - file: fabio-install

fabio-setcap:
  cmd.run:
    - name: "setcap cap_net_bind_service=+ep /usr/local/bin/fabio-{{ fabio.version }}"
    - onchanges:
      - file: fabio-install
