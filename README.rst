======
fabio
======

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Available states
================

.. contents::
    :local:

``fabio``
------------

Installs and configures the fabio service.

``fabio.install``
------------------

Downloads and installs the fabio binary file.

``fabio.config``
-----------------

Provision the fabio configuration files and sources.

``fabio.service``
------------------

Adds the fabio service startup configuration or script to an operating system.

To start a service during Salt run and enable it at boot time, you need to set following Pillar:

.. code:: yaml

    fabio:
      service: True

``fabio-template``
-------------------

Installs and configures fabio template.

.. vim: fenc=utf-8 spell spl=en cc=100 tw=99 fo=want sts=4 sw=4 et
