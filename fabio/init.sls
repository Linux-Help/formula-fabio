{% from slspath+"/map.jinja" import fabio with context %}

include:
  - {{ slspath }}.install
  - {{ slspath }}.config
  - {{ slspath }}.service
